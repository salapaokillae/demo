package com.example.demo.client;

public interface AuthenticationAdapter {
    boolean verify(String token);
}
