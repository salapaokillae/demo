package com.example.demo.client.impl;

import com.example.demo.client.AuthenticationAdapter;
import com.example.demo.dto.VerifyTokenRequest;
import com.example.demo.dto.VerifyTokenResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Slf4j
@Service
public class AuthenticationAdapterImpl implements AuthenticationAdapter {
    @Value("${endpoint.authentication.baseurl}")
    private String baseUrl;

    @Value("${endpoint.authentication.path.verify}")
    private String pathVerify;

    @Override
    public boolean verify(String token) {
        var isValid = false;
        var body = VerifyTokenRequest.builder()
                .token(token)
                .build();
        try {
            var restTemplate = new RestTemplate();
            var response = restTemplate.postForEntity(baseUrl + pathVerify,
                    body,
                    VerifyTokenResponse.class);
            if (response.getStatusCode().is2xxSuccessful() && response.getBody() != null) {
                isValid = response.getBody().isValid();
                log.info("verify token result is {}", isValid);
            }
        } catch (Exception e) {
            log.error("error during call authentication-verify service", e);
        }
        return isValid;
    }
}
