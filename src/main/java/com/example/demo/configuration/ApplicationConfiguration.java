package com.example.demo.configuration;

import com.example.demo.interceptor.AuthenticationInterceptor;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Component
public class ApplicationConfiguration implements WebMvcConfigurer {

    private AuthenticationInterceptor authenticationInterceptor;

    public ApplicationConfiguration(AuthenticationInterceptor authenticationInterceptor) {
        this.authenticationInterceptor = authenticationInterceptor;
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(authenticationInterceptor)
                .excludePathPatterns("/authentication/**",
                        // bypass auth validation for swagger
                        "/webjars/**", "/v2/api-docs/**", "/swagger-resources/**", "/swagger-ui.html/**");
    }
}
