package com.example.demo.constant;

public class ErrorConstant {
    public static final String ERR_001 = "E001";    // Common error
    public static final String ERR_002 = "E002";    // Validation error
    public static final String ERR_100 = "E100";    // Business error
    public static final String ERR_200 = "E200";    // Authentication error

}
