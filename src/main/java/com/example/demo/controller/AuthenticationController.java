package com.example.demo.controller;

import com.example.demo.constant.ErrorConstant;
import com.example.demo.dto.*;
import com.example.demo.exception.BusinessException;
import com.example.demo.service.AuthenticationService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Slf4j
@RestController
@RequestMapping("authentication")
public class AuthenticationController {

    private AuthenticationService authenticationService;

    public AuthenticationController(AuthenticationService authenticationService) {
        this.authenticationService = authenticationService;
    }

    @ApiOperation(value = "For authenticate user with username and password (username: test, password: 123456)")
    @PostMapping("/login")
    public ResponseEntity<BaseResponse<String>> login(@RequestBody @Valid LoginRequest request) {
        var httpStatus = HttpStatus.OK;
        var errorCode = "";
        String responseData = null;
        try {
            // username = test, password = 123456
            responseData = authenticationService.authenticate(request.getUsername(), request.getPassword());
        } catch (BusinessException e) {
            httpStatus = HttpStatus.UNAUTHORIZED;
            errorCode = ErrorConstant.ERR_200;
        } catch (Exception e) {
            httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
            errorCode = ErrorConstant.ERR_001;
        }
        return new ResponseEntity<>(BaseResponse
                .<String>builder()
                .errorCode(errorCode)
                .data(responseData)
                .build(), httpStatus);
    }

    @PostMapping("/verify")
    public ResponseEntity<VerifyTokenResponse> verify(@RequestBody @Valid VerifyTokenRequest request) {
        var httpStatus = HttpStatus.OK;
        var isValid = authenticationService.verify(request.getToken());
        var body = VerifyTokenResponse.builder().valid(isValid).build();
        if (!isValid) {
           httpStatus = HttpStatus.UNAUTHORIZED;
        }
        return new ResponseEntity(body, httpStatus);
    }
}
