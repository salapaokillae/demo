package com.example.demo.controller;

import com.example.demo.constant.ErrorConstant;
import com.example.demo.dto.BaseResponse;
import com.example.demo.dto.CreateEmployeeRequest;
import com.example.demo.dto.EmployeeDto;
import com.example.demo.dto.UpdateEmployeeRequest;
import com.example.demo.exception.BusinessException;
import com.example.demo.service.EmployeeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("employee")
public class EmployeeController {

    private EmployeeService employeeService;

    public EmployeeController(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @GetMapping
    public ResponseEntity<BaseResponse<List<EmployeeDto>>> getAll() {
        var httpStatus = HttpStatus.OK;
        var errorCode = "";
        List<EmployeeDto> responseData = null;
        try {
            responseData = employeeService.getAll();
        } catch (Exception e) {
            httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
            errorCode = ErrorConstant.ERR_001;
            log.error("error during process get all employee: ", e);
        }
        return new ResponseEntity<>(BaseResponse
                .<List<EmployeeDto>>builder()
                .errorCode(errorCode)
                .data(responseData)
                .build(), httpStatus);
    }

    @GetMapping("/{id}")
    public ResponseEntity<BaseResponse<EmployeeDto>> getById(@PathVariable Integer id) {
        var httpStatus = HttpStatus.OK;
        var errorCode = "";
        EmployeeDto responseData = null;
        try {
            responseData = employeeService.getById(id);
        } catch (BusinessException e) {
            httpStatus = HttpStatus.CONFLICT;
            errorCode = ErrorConstant.ERR_100;
        } catch (Exception e) {
            httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
            errorCode = ErrorConstant.ERR_001;
            log.error("error during process get employee by id: {}", id, e);
        }
        return new ResponseEntity<>(BaseResponse
                .<EmployeeDto>builder()
                .errorCode(errorCode)
                .data(responseData)
                .build(), httpStatus);
    }

    @PostMapping
    public ResponseEntity<BaseResponse<EmployeeDto>> insert(@RequestBody @Valid CreateEmployeeRequest request) {
        var httpStatus = HttpStatus.OK;
        var errorCode = "";
        EmployeeDto responseData = null;
        try {
            responseData = employeeService.insert(request);
        } catch (Exception e) {
            httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
            errorCode = ErrorConstant.ERR_001;
            log.error("error during process insert employee: {}", request, e);
        }
        return new ResponseEntity<>(BaseResponse
                .<EmployeeDto>builder()
                .errorCode(errorCode)
                .data(responseData)
                .build(), httpStatus);
    }

    @PatchMapping
    public ResponseEntity<BaseResponse<EmployeeDto>> update(@RequestBody @Valid UpdateEmployeeRequest request) {
        var httpStatus = HttpStatus.OK;
        var errorCode = "";
        EmployeeDto responseData = null;
        try {
            responseData = employeeService.update(request);
        } catch (BusinessException e) {
            httpStatus = HttpStatus.CONFLICT;
            errorCode = ErrorConstant.ERR_100;
            log.error("error during process update employee: {}", request, e);
        } catch (Exception e) {
            httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
            errorCode = ErrorConstant.ERR_001;
            log.error("error during process update employee: {}", request, e);
        }
        return new ResponseEntity<>(BaseResponse
                .<EmployeeDto>builder()
                .errorCode(errorCode)
                .data(responseData)
                .build(), httpStatus);
    }

    @PostMapping("/delete/{id}")
    public ResponseEntity<BaseResponse<EmployeeDto>> deleteById(@PathVariable Integer id) {
        var httpStatus = HttpStatus.OK;
        var errorCode = "";
        try {
            employeeService.delete(id);
        } catch(EmptyResultDataAccessException e) {
            httpStatus = HttpStatus.BAD_REQUEST;
            errorCode = ErrorConstant.ERR_100;
            log.error("employee id: {} is not exist: ", id, e);
        } catch (Exception e) {
            httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
            errorCode = ErrorConstant.ERR_001;
            log.error("error during process delete employee: ", e);
        }
        return new ResponseEntity<>(BaseResponse
                .<EmployeeDto>builder()
                .errorCode(errorCode)
                .data(null)
                .build(), httpStatus);
    }
}
