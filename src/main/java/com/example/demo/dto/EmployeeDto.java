package com.example.demo.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EmployeeDto {
    private Integer employeeId;

    private String firstName;

    private String lastName;

    private String createdDate;

    private String createdBy;

    private String updatedDate;

    private String updatedBy;
}
