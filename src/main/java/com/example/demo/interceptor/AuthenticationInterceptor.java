package com.example.demo.interceptor;

import com.example.demo.client.AuthenticationAdapter;
import com.example.demo.constant.ErrorConstant;
import com.example.demo.dto.BaseResponse;
import com.example.demo.service.AuthenticationService;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Slf4j
@Component
public class AuthenticationInterceptor implements HandlerInterceptor {

    private AuthenticationAdapter authenticationAdapter;

    public AuthenticationInterceptor(AuthenticationAdapter authenticationAdapter) {
        this.authenticationAdapter = authenticationAdapter;
    }

    @Override
    public boolean preHandle(
            HttpServletRequest request,
            HttpServletResponse response,
            Object handler) throws Exception {
        var authorizationHeader = request.getHeader(HttpHeaders.AUTHORIZATION);
        if (StringUtils.isEmpty(authorizationHeader)) {
            log.error("error authorization header is empty");
            return responseUnAuthorize(response);
        }

        // calling authentication service (now we implement same service)
        var jwtToken = authorizationHeader.replace("Bearer ", "");
        if (!authenticationAdapter.verify(jwtToken)) {
            log.error("error token invalid or expire");
            return responseUnAuthorize(response);
        }
        return true;
    }

    private boolean responseUnAuthorize(HttpServletResponse response) throws IOException {
        var mapper = new ObjectMapper();
        var body = BaseResponse.builder()
                .errorCode(ErrorConstant.ERR_200)
                .build();
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        response.getWriter().write(mapper.writeValueAsString(body));
        return false;
    }
}