package com.example.demo.mapper;

import com.example.demo.dto.CreateEmployeeRequest;
import com.example.demo.dto.EmployeeDto;
import com.example.demo.entity.Employee;
import com.example.demo.utils.CommonDateUtils;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.mapstruct.factory.Mappers;

import java.time.LocalDateTime;

@Mapper(imports = {LocalDateTime.class})
public interface EmployeeMapper {

    EmployeeMapper INSTANCE = Mappers.getMapper( EmployeeMapper.class );

    @Mapping(target = "createdBy", constant = "DEFAULT_USER")
    @Mapping(target = "createdDate", expression ="java( LocalDateTime.now() )")
    Employee toEntity(CreateEmployeeRequest dto);

    @Mapping(target = "createdDate", source = "createdDate", qualifiedByName = "formatDate")
    @Mapping(target = "updatedDate", source = "updatedDate", qualifiedByName = "formatDate")
    EmployeeDto toDto(Employee entity);

    @Named("formatDate")
    static String formatDate(LocalDateTime dateTime) {
        return CommonDateUtils.toString(dateTime);
    }
}
