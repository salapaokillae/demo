package com.example.demo.service;

public interface AuthenticationService {
    String authenticate(String username, String password);
    boolean verify(String token);
}
