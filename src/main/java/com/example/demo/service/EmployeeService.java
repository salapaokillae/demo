package com.example.demo.service;

import com.example.demo.dto.CreateEmployeeRequest;
import com.example.demo.dto.EmployeeDto;
import com.example.demo.dto.UpdateEmployeeRequest;

import java.util.List;

public interface EmployeeService {
    List<EmployeeDto> getAll();
    EmployeeDto getById(Integer id);
    EmployeeDto insert(CreateEmployeeRequest dto);
    EmployeeDto update(UpdateEmployeeRequest dto);
    void delete(Integer id);
}
