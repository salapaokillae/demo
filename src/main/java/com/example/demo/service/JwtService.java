package com.example.demo.service;

public interface JwtService {
    String generateToken(Integer userId);
    boolean verifyToken(String jwtToken);
}
