package com.example.demo.service.impl;

import com.example.demo.exception.BusinessException;
import com.example.demo.repository.UserProfileRepository;
import com.example.demo.service.AuthenticationService;
import com.example.demo.service.JwtService;
import com.example.demo.utils.PasswordEncoder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class AuthenticationServiceImpl implements AuthenticationService {

    private JwtService jwtService;
    private UserProfileRepository userProfileRepository;

    public AuthenticationServiceImpl(JwtService jwtService, UserProfileRepository userProfileRepository) {
        this.jwtService = jwtService;
        this.userProfileRepository = userProfileRepository;
    }

    @Override
    public String authenticate(String username, String password) {
        var userProfile = userProfileRepository.findByUsernameAndPassword(username, PasswordEncoder.encode(password));
        if (userProfile == null) {
            throw new BusinessException("invalid username or password");
        }
        log.info("found user-profile id: " + userProfile.getUserId());
        return jwtService.generateToken(userProfile.getUserId());
    }

    @Override
    public boolean verify(String token) {
        return jwtService.verifyToken(token);
    }
}
