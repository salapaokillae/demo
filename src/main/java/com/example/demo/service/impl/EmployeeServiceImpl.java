package com.example.demo.service.impl;

import com.example.demo.dto.CreateEmployeeRequest;
import com.example.demo.dto.EmployeeDto;
import com.example.demo.dto.UpdateEmployeeRequest;
import com.example.demo.exception.BusinessException;
import com.example.demo.mapper.EmployeeMapper;
import com.example.demo.repository.EmployeeRepository;
import com.example.demo.service.EmployeeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
public class EmployeeServiceImpl implements EmployeeService {
    private EmployeeRepository employeeRepository;

    public EmployeeServiceImpl(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    @Override
    public List<EmployeeDto> getAll() {
        var data = employeeRepository.findAll()
                .stream()
                .map(EmployeeMapper.INSTANCE::toDto).collect(Collectors.toList());
        log.info("found employee: " + data.size() + " records");
        return data;
    }

    @Override
    public EmployeeDto getById(Integer id) {
        var optionalEntity = employeeRepository.findById(id);
        if(optionalEntity.isEmpty()) {
            log.error("employee id {} not found", id);
            throw new BusinessException("employee not found");
        }
        var entity = optionalEntity.get();
        log.info("found employee: {}", entity);
        return EmployeeMapper.INSTANCE.toDto(entity);
    }

    @Override
    public EmployeeDto insert(CreateEmployeeRequest dto) {
        var entity = EmployeeMapper.INSTANCE.toEntity(dto);
        var result = employeeRepository.save(entity);
        return EmployeeMapper.INSTANCE.toDto(result);
    }

    @Override
    public EmployeeDto update(UpdateEmployeeRequest dto) {
        var employeeId = dto.getEmployeeId();
        var optionalEntity = employeeRepository.findById(dto.getEmployeeId());
        if(optionalEntity.isEmpty()) {
            log.error("employee id {} not found", employeeId);
            throw new BusinessException("employee not found");
        }
        var entity = optionalEntity.get();
        entity.setFirstName(dto.getFirstName());
        entity.setLastName(dto.getLastName());
        entity.setUpdatedBy("DEFAULT_USER");
        entity.setUpdatedDate(LocalDateTime.now());
        var result = employeeRepository.save(entity);
        return EmployeeMapper.INSTANCE.toDto(result);
    }

    @Override
    public void delete(Integer id) {
        employeeRepository.deleteById(id);
    }
}
