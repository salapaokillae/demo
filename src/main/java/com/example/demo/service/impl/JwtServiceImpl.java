package com.example.demo.service.impl;

import com.example.demo.service.JwtService;
import io.jsonwebtoken.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.*;

@Slf4j
@Service
public class JwtServiceImpl implements JwtService {

    @Value("${jwt.private-key}")
    private String privateKey;

    @Value("${jwt.public-key}")
    private String publicKey;

    @Value("${jwt.duration-minute}")
    private Integer durationMinute;

    @Override
    public String generateToken(Integer userId) {
        String token = null;
        try {
            Map<String, Object> claims = new HashMap<>();
            var now = Calendar.getInstance().getTime();
            var expire = DateUtils.addMinutes(now, durationMinute);
            claims.put("uid", userId);
            token = Jwts.builder()
                    .setClaims(claims)
                    .setId(UUID.randomUUID().toString())
                    .setIssuedAt(now)
                    .setExpiration(expire)
                    .signWith(SignatureAlgorithm.RS256, getPrivateKey(privateKey)).compact();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return token;
    }

    private PrivateKey getPrivateKey(String privateKey) throws Exception {
        privateKey = privateKey.replace("-----BEGIN PRIVATE KEY-----", "");
        privateKey = privateKey.replace("-----END PRIVATE KEY-----", "");
        privateKey = privateKey.replaceAll("\\s+","");
        PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(Base64.getDecoder().decode(privateKey));
        KeyFactory kf = KeyFactory.getInstance("RSA");
        return kf.generatePrivate(keySpec);
    }

    private PublicKey getPublicKey(String publicKey) throws Exception {
        publicKey = publicKey.replace("-----BEGIN PUBLIC KEY-----", "");
        publicKey = publicKey.replace("-----END PUBLIC KEY-----", "");
        publicKey = publicKey.replaceAll("\\s+","");
        byte[] publicBytes = Base64.getDecoder().decode(publicKey);
        X509EncodedKeySpec keySpec = new X509EncodedKeySpec(publicBytes);

        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        return keyFactory.generatePublic(keySpec);
    }

    @Override
    public boolean verifyToken(String jwtToken) {
        var isValid = false;
        try {
            Jwts.parser().setSigningKey(getPublicKey(publicKey)).parseClaimsJws(jwtToken).getBody();
            isValid = true;
        } catch (SignatureException e) {
            log.error("invalid token: ", e);
        } catch (ExpiredJwtException e) {
            log.error("token expired: ", e);
        } catch (Exception e) {
            log.error("error verifyToken: ", e);
        }
        return isValid;
    }
}
