package com.example.demo.utils;

import java.util.Base64;

public class PasswordEncoder {
    public static String encode(String plainText) {
        // TODO we can change encoder type in future just for demo
        return Base64.getEncoder().encodeToString(plainText.getBytes());
    }

    public static String decoder(String encryptedText) {
        byte[] decodedBytes = Base64.getDecoder().decode(encryptedText);
        return new String(decodedBytes);
    }
}
