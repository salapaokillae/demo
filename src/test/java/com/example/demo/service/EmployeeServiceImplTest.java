package com.example.demo.service;

import com.example.demo.dto.CreateEmployeeRequest;
import com.example.demo.dto.UpdateEmployeeRequest;
import com.example.demo.entity.Employee;
import com.example.demo.exception.BusinessException;
import com.example.demo.repository.EmployeeRepository;
import com.example.demo.service.impl.EmployeeServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@Slf4j
@ExtendWith(MockitoExtension.class)
public class EmployeeServiceImplTest {

    @InjectMocks
    private EmployeeServiceImpl employeeService;

    @Mock
    private EmployeeRepository employeeRepository;

    @Captor
    private ArgumentCaptor<Employee> employeeArgumentCaptor;

    @Captor
    private ArgumentCaptor<Integer> idArgumentCaptor;

    @BeforeEach
    public void setUp() {

    }

    @Test
    public void WhenCallGetAllThenSuccess() {
        var now = LocalDateTime.now();
        var foundEntity = Employee.builder()
                .firstName("athinun")
                .lastName("chalassathien")
                .createdBy("TEST")
                .createdDate(now)
                .updatedBy("UPDATE_TEST")
                .updatedDate(now)
                .build();
        Mockito.when(employeeRepository.findAll()).thenReturn(List.of(foundEntity));
        var result = employeeService.getAll();
        Assertions.assertEquals(1, result.size());

        var singleResult = result.get(0);
        Assertions.assertEquals(foundEntity.getFirstName(), singleResult.getFirstName());
        Assertions.assertEquals(foundEntity.getLastName(), singleResult.getLastName());
        Assertions.assertEquals(foundEntity.getCreatedBy(), singleResult.getCreatedBy());
        Assertions.assertEquals(foundEntity.getUpdatedBy(), singleResult.getUpdatedBy());
    }

    @Test
    public void WhenCallGetByIdThenSuccess() {
        var now = LocalDateTime.now();
        var foundEntity = Employee.builder()
                .firstName("athinun")
                .lastName("chalassathien")
                .createdBy("TEST")
                .createdDate(now)
                .updatedBy("UPDATE_TEST")
                .updatedDate(now)
                .build();

        Mockito.when(employeeRepository.findById(1)).thenReturn(Optional.of(foundEntity));
        var singleResult = employeeService.getById(1);
        Assertions.assertNotNull(singleResult);
        Assertions.assertEquals(foundEntity.getFirstName(), singleResult.getFirstName());
        Assertions.assertEquals(foundEntity.getLastName(), singleResult.getLastName());
        Assertions.assertEquals(foundEntity.getCreatedBy(), singleResult.getCreatedBy());
        Assertions.assertEquals(foundEntity.getUpdatedBy(), singleResult.getUpdatedBy());
    }

    @Test
    public void WhenCallInsertThenSuccess() {
        var input = CreateEmployeeRequest.builder()
                .firstName("athinun")
                .lastName("chalassathien")
                .build();
        var savedEntity = Employee.builder()
                .firstName("athinun")
                .lastName("chalassathien")
                .createdBy("TEST")
                .createdDate(LocalDateTime.now())
                .build();
        Mockito.when(employeeRepository.save(Mockito.any())).thenReturn(savedEntity);
        var singleResult = employeeService.insert(input);
        Assertions.assertNotNull(singleResult);

        verify(employeeRepository, times(1)).save(employeeArgumentCaptor.capture());
        var captorValue = employeeArgumentCaptor.getValue();
        Assertions.assertEquals(input.getFirstName(), captorValue.getFirstName());
        Assertions.assertEquals(input.getLastName(), captorValue.getLastName());

        Assertions.assertEquals(input.getFirstName(), savedEntity.getFirstName());
        Assertions.assertEquals(input.getLastName(), savedEntity.getLastName());
    }

    @Test
    public void WhenCallUpdateThenSuccess() {
        var input = UpdateEmployeeRequest.builder()
                .employeeId(1)
                .firstName("athinun2")
                .lastName("chalassathien2")
                .build();
        var foundEntity = Employee.builder()
                .employeeId(1)
                .firstName("athinun")
                .lastName("chalassathien")
                .createdBy("TEST")
                .createdDate(LocalDateTime.now())
                .build();
        var updatedEntity = foundEntity.toBuilder()
                .firstName("athinun2")
                .lastName("chalassathien2")
                .createdBy("TEST")
                .createdDate(LocalDateTime.now())
                .updatedBy("DEFAULT_USER")
                .updatedDate(LocalDateTime.now())
                .build();
        Mockito.when(employeeRepository.findById(Mockito.anyInt())).thenReturn(Optional.of(foundEntity));
        Mockito.when(employeeRepository.save(Mockito.any())).thenReturn(updatedEntity);
        var singleResult = employeeService.update(input);
        Assertions.assertNotNull(singleResult);

        verify(employeeRepository, times(1)).save(employeeArgumentCaptor.capture());
        var captorValue = employeeArgumentCaptor.getValue();
        Assertions.assertEquals(input.getEmployeeId(), captorValue.getEmployeeId());
        Assertions.assertEquals(input.getFirstName(), captorValue.getFirstName());
        Assertions.assertEquals(input.getLastName(), captorValue.getLastName());

        Assertions.assertEquals(updatedEntity.getFirstName(), singleResult.getFirstName());
        Assertions.assertEquals(updatedEntity.getLastName(), singleResult.getLastName());
        Assertions.assertEquals("DEFAULT_USER", singleResult.getUpdatedBy());
        Assertions.assertNotNull(singleResult.getUpdatedDate());
    }

    @Test
    public void GivingEmployeeNotExistDuringUpdateThenFail() {
        var input = UpdateEmployeeRequest.builder()
                .employeeId(1)
                .firstName("athinun")
                .lastName("chalassathien")
                .build();
        Mockito.when(employeeRepository.findById(Mockito.anyInt())).thenReturn(Optional.empty());
        assertThatThrownBy(() -> employeeService.update(input)).isInstanceOf(BusinessException.class);
    }

    @Test
    public void WhenCallDeleteThenSuccess() {
        employeeService.delete(1);
        verify(employeeRepository, times(1)).deleteById(idArgumentCaptor.capture());
        var captorValue = idArgumentCaptor.getValue();
        Assertions.assertEquals(1, captorValue);
    }
}
