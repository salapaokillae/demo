package com.example.demo.service;

import com.example.demo.service.impl.JwtServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.util.ReflectionTestUtils;

@Slf4j
@ExtendWith(MockitoExtension.class)
public class JwtServiceImplTest {

    @InjectMocks
    private JwtServiceImpl jwtService;

    @BeforeEach
    public void setUp() {
        var privateKey = "-----BEGIN PRIVATE KEY-----\n" +
                "MIICeAIBADANBgkqhkiG9w0BAQEFAASCAmIwggJeAgEAAoGBAOSjKe6EMdx8DqpN\n" +
                "VktjeV72OZdk+lc71jVlY3Swu2c5iGlvHM1gfI/QsBmiUsQQst98jtTnXQcela+N\n" +
                "a0+K6Vb9aIBipC/pxPD2Wntb4kPAPpuokc2FsTpMQc+Jc9Su4maasxpmawtoAx5P\n" +
                "5uNglcVNH6TS8U/h4jUKdcuUyYt9AgMBAAECgYEAokd9F6IUkd4ruoyx10h/ePti\n" +
                "3SX62bfKzpNsCAL4wMeBHvSSiOMXBiylEUy5WhhQHXgXGN4ulUMME1JY/FgoTRKW\n" +
                "2omBxnNwpKE9j51dY2uG7VQb309eGd+f3+ibSS734HR5Zm92/mPv+mHs6jYb3F/q\n" +
                "5L7QT0Zt8qhAB7s5XIECQQDzV2PUg5mjM7EzvYNURyYrNvOifLx983HsC3qgTmTs\n" +
                "+Q9vppcqyzZSeIaQcCQTQ7GAG+I7lm5P/E+X9HsbEcEVAkEA8If1Y/Jm6sCUIRW7\n" +
                "ipVSUo6TNDHizh+Eipn4JDEo/gaUA0JuRVi9Ohfuc0cK1lLsFsPwJpJjhmwLDpdU\n" +
                "EV2qyQJBAMrY02Aon577DUFLPjm34apucWPGxT3c/N8+pmcRUnJ0H5j77tF+enb+\n" +
                "/3VR9dCVQla/Z7QybxyAAD10UFa9Uw0CQQCjtRV6Uhhj/HCVWBJNzzW5hNqBstZY\n" +
                "TjFKmGLAV0k8I8QHt+YPgsc/y9v6T8b7enK/R9qilg3xBoY8BxkrvKGpAkA2qlQ+\n" +
                "9Vj8Y65MPM3NufJhl24hsrC6kc92EXnEEiOZ/spRSukwbkmB24TY03rfaekINIIc\n" +
                "jxu2UWUnPboubINP\n" +
                "-----END PRIVATE KEY-----";
        var publicKey = "-----BEGIN PUBLIC KEY-----\n" +
                "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDkoynuhDHcfA6qTVZLY3le9jmX\n" +
                "ZPpXO9Y1ZWN0sLtnOYhpbxzNYHyP0LAZolLEELLffI7U510HHpWvjWtPiulW/WiA\n" +
                "YqQv6cTw9lp7W+JDwD6bqJHNhbE6TEHPiXPUruJmmrMaZmsLaAMeT+bjYJXFTR+k\n" +
                "0vFP4eI1CnXLlMmLfQIDAQAB\n" +
                "-----END PUBLIC KEY-----";
        ReflectionTestUtils.setField(jwtService, "privateKey", privateKey);
        ReflectionTestUtils.setField(jwtService, "publicKey", publicKey);
        ReflectionTestUtils.setField(jwtService, "durationMinute", 15);
    }

    @Test
    public void GivingCorrectKeyThenGenerateAndVerifyTokenSuccess() {
        var token = jwtService.generateToken(1);
        log.info("token : {}", token);
        var isValid = jwtService.verifyToken(token);
        log.info("isValid : {}", isValid);
        Assertions.assertTrue(isValid);
    }

    @Test
    public void GivingInCorrectTokenThenVerifyTokenFail() {
        var invalidToken = "mock-invalid-token-123456";
        var isValid = jwtService.verifyToken(invalidToken);
        log.info("isValid : {}", isValid);
        Assertions.assertFalse(isValid);
    }
}
